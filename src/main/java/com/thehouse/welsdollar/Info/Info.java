package com.thehouse.welsdollar.Info;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "info")
public class Info {
	
	@JsonProperty("info_id")
	private int infoID;
	
	@JsonProperty("amount")
	private int amount;
	
	@JsonProperty("church")
	private String church;
	
	@JsonProperty("link")
	private String link;
	
	public Info() {
		
	}
	
	public Info(int amount, String church, String link) {
		this.amount = amount;
		this.church = church;
		this.link = link;
	}
	
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Column(name = "info_id")
	public int getInfoID() {
		return infoID;
	}
	
	@Column(name = "amount")
	public int getAmount() {
		return amount;
	}
	
	@Column(name = "church")
	public String getChurch() {
		return church;
	}
	
	@Column(name = "link")
	public String getLink() {
		return link;
	}
	
	public void setInfoID(int infoID) {
		this.infoID = infoID;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public void setChurch(String church) {
		this.church = church;
	}
	
	public void setLink(String link) {
		this.link = link;
	}
	
}
