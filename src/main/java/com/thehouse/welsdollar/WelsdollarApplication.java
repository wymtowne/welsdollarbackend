package com.thehouse.welsdollar;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

import com.thehouse.welsdollar.Info.InfoService;

@SpringBootApplication
public class WelsdollarApplication {

	public static void main(String[] args) {
		SpringApplication.run(WelsdollarApplication.class, args);
	}
	
	@Bean
	public TaskExecutor taskExecutor() {
	    return new SimpleAsyncTaskExecutor(); // Or use another one of your liking
	}
	
	@Bean
	public CommandLineRunner schedulingReset(TaskExecutor executor) {
	    return new CommandLineRunner() {
	    	
	    	@Autowired
	    	InfoService infoService;
	    	
	        public void run(String... args) throws Exception {
	            executor.execute(new Runnable() {
	    			
	    			public void run() {
	    		       
	    		        long msTillTimeout;
	    				
	    				while(true) {
	    					
	    					Calendar c = Calendar.getInstance();

	    					if(c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY && c.get(Calendar.HOUR_OF_DAY) < 12) {

	    						infoService.resetAmount();
	    						
	    						while(c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY && c.get(Calendar.HOUR_OF_DAY) < 12) {
	    							//Loop until out of inactive period
	    						}
	    					}
	    				
	    					c = null;
	    					
	    				}
	    				
	    			}
	    			
	    			});
	        }
	    };
	}

	//START HERE NEXT TIME AND RUN RESET EVERY SATURDAY AT MIDNIGHT
	/*public static void startResetThread() {
		
		Thread resetThread = new Thread(new Runnable() {
		
		public void run() {
			
			Calendar c = Calendar.getInstance();
	       
	        long msTillTimeout;
			
			while(true) {
			
				setToNextSaturday(Calendar.SATURDAY, c);
				msTillTimeout = (c.getTimeInMillis()-System.currentTimeMillis());
				
				try {
					Thread.sleep(msTillTimeout);
					
					while(true) {
						
					}
					
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
			
		}
		
		});
	
		resetThread.start();
	}
	
	public static void setToNextSaturday(int dayOfWeekToSet, Calendar c) {
			int currentDayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			System.out.println(c.DAY_OF_MONTH);
			System.out.println(currentDayOfWeek);
			
			while(currentDayOfWeek != dayOfWeekToSet) {
				c.add(Calendar.DAY_OF_MONTH, 1);
				currentDayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			}
			
			System.out.println(c.DAY_OF_WEEK);
	}*/
	
}
